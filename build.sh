#!/usr/bin/env sh

SCRIPTPATH=$(readlink -f "$0")
THISDIR=$(dirname "$SCRIPTPATH")
PATCHDIR=${THISDIR}/patches
PKGDIR=${THISDIR}/pkg.d
SCRIPTDIR=${THISDIR}/script.d

IMGMOUNT="/mnt/alpine"
IMGFILE=${1:-alpine.qcow2}
IMGSIZE=${IMGSIZE:-512M}
EFIPARTSIZE_MB=148
NBDDEV=/dev/nbd0

if [[ $(id -u) -ne 0 ]]; then
	echo "Please run this script with superuser privledges"
	exit
fi

ALPINE_TAR=${ALPINE_TAR:-alpine-minirootfs-3.8.0-aarch64.tar.gz}
KERNEL_TAR=${KERNEL_TAR:-kernel.tar.gz}

cleanup () {
	for dir in "${IMGMOUNT}/boot" \
		   "${IMGMOUNT}/proc" \
		   "${IMGMOUNT}/sys" \
		   "${IMGMOUNT}/dev" \
		   "${IMGMOUNT}"
	do
		if mount | grep $dir > /dev/null; then
			umount $dir
		fi
	done
	qemu-nbd -d ${NBDDEV}

}

error () {
    echo "ERROR: $1"
    cleanup
    exit
}

partition () {
    parted --script ${1} \
        mklabel gpt \
        mkpart boot fat32 0% ${EFIPARTSIZE_MB} \
        mkpart system ext4 ${EFIPARTSIZE_MB} 100% \
        set 1 boot on \
        set 1 esp on 


    partx -a ${1}

    # format partitions
    mkfs.fat -F 32 ${1}p1
    mkfs.ext4 ${1}p2 

    mkdir -p ${IMGMOUNT}
    mount ${1}p2 ${IMGMOUNT}

    # mount boot partion
    mkdir -p ${IMGMOUNT}/boot/
    mount ${1}p1 ${IMGMOUNT}/boot/
}

install_root () {
    # Copy alpine rootfs
    tar -C ${1}/ -zxvpf ${ALPINE_TAR} 
    # Copy the kernel
    tar -C ${1}/ -zxvpf ${KERNEL_TAR} 
}

setup_grub () {
    touch ${IMGMOUNT}/etc/update-extlinux.conf
    mkdir -p ${IMGMOUNT}/etc/default/
    ROOT_DEVICE=`blkid ${NBDDEV}p2 -o value -s PARTUUID`
    cat <<EOF > ${IMGMOUNT}/etc/default/grub
GRUB_CMDLINE_LINUX_DEFAULT="console=ttyAMA0,115200 init=/sbin/init rootwait rw"
GRUB_DEVICE_UUID=${ROOT_DEVICE}
GRUB_TERMINAL="serial"
GRUB_SERIAL_COMMAND="serial --speed=115200 --word=8 --parity=no --stop=1 efi0"
EOF

    cat ${IMGMOUNT}/etc/default/grub
    ls -la ${IMGMOUNT}/lib/modules/
    sleep 3

    mkdir -p ${IMGMOUNT}/boot/EFI/boot/
    # By default, grub will search for grub.cfg in the same folder as EFI $cmdpath, i.e EFI/boot/
    chroot ${IMGMOUNT} /bin/sh -l -c 'grub-mkconfig -o /boot/grub/grub.cfg'
    # As grub doesn't want to generate with a UUID (even when specified, force it)
    sed -i "s/root=\/dev\/nbd0p2/root=PARTUUID=${ROOT_DEVICE}/g" ${IMGMOUNT}/boot/grub/grub.cfg
    grep "PARTUUID=${ROOT_DEVICE}" ${IMGMOUNT}/boot/grub/grub.cfg
    if [[ "$?" -ne 0 ]]; then
        echo "ERROR: PARTUUID is not in the generated grub.cfg, the script may need to be adjusted"
        echo "NOTE: ${IMGMOUNT} is still mounted for debugging purposes"
        exit
    fi

    GRUB_DEVICE=`blkid /dev/${NBDDEV}p1 -o value -s UUID`
    echo "GRUB device is ${GRUB_DEVICE}"

    # In a perfect world, grub-install could sort this, but it isn't working for me here
    cat <<EOF > ${IMGMOUNT}/boot/grub/search.cfg
search --set=root --fs-uuid ${GRUB_DEVICE} --hint hd0,gpt1
prefix=(\$root)/grub
EOF

    cat <<'EOF' > ${IMGMOUNT}/install-grub.sh
grub-mkimage -c /boot/grub/search.cfg --format=arm64-efi -d /usr/lib/grub/arm64-efi --output=/boot/EFI/boot/bootaa64.efi -p "" \
/usr/lib/grub/arm64-efi/part_gpt \
/usr/lib/grub/arm64-efi/disk /usr/lib/grub/arm64-efi/search \
/usr/lib/grub/arm64-efi/search_fs_uuid /usr/lib/grub/arm64-efi/fshelp \
/usr/lib/grub/arm64-efi/search_label /usr/lib/grub/arm64-efi/fat
EOF
    cat ${IMGMOUNT}/install-grub.sh
    chroot ${IMGMOUNT} /bin/sh -l -c "sh /install-grub.sh"
    rm ${IMGMOUNT}/install-grub.sh
}

modprobe nbd
qemu-img create -f qcow2 ${IMGFILE} ${IMGSIZE}
qemu-nbd -c ${NBDDEV} ${IMGFILE}
partition ${NBDDEV} ||  error "Error partioning device!" 

install_root ${IMGMOUNT} || error "Error installing rootfs and kernel!" 


#perpare for chroot
mount --bind /dev ${IMGMOUNT}/dev/
mount --bind /sys ${IMGMOUNT}/sys/
mount -t proc none ${IMGMOUNT}/proc/
cp /etc/resolv.conf ${IMGMOUNT}/etc/

# install packages
chroot ${IMGMOUNT} /bin/sh -l -c 'apk update && apk upgrade'
for file in ${PKGDIR}/*; do
    pkgs=$(cat $file | tr '\n' ' ')
    chroot ${IMGMOUNT} /bin/sh -c -l "apk add $pkgs"
done

# run scripts in the chrooted environment
for file in ${SCRIPTDIR}/*; do
    while read line; do 
        chroot ${IMGMOUNT} /bin/sh -c -l "$line"
    done < $file
done
echo "alpine" > ${IMGMOUNT}/etc/hostname

cat <<EOF > ${IMGMOUNT}/etc/network/interfaces
auto eth0
iface eth0 inet dhcp
EOF

echo "rtc-efi" >> ${IMGMOUNT}/etc/modules
echo "virtio-rng" >> ${IMGMOUNT}/etc/modules

# apply patches
for file in ${PATCHDIR}/*.patch; do
    patch -d ${IMGMOUNT} -p1 -i $file
done

setup_grub || error "Error setting up grub!"

echo "::askfirst:-/bin/sh" >> ${IMGMOUNT}/etc/inittab

# Teardown
cleanup
