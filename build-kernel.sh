#!/bin/bash
KERNEL_DIR=linux-next
SCRIPT_DIR=`pwd`
ROOT_DIR=`pwd`/rootfs

rm -fr $ROOT_DIR
mkdir -p $ROOT_DIR/boot
cd $KERNEL_DIR
make ARCH=arm64 CROSS_COMPILE=aarch64-unknown-linux-gnueabi- -j`nproc` all
make ARCH=arm64 INSTALL_PATH=$ROOT_DIR/boot install
make INSTALL_MOD_PATH=$ROOT_DIR modules_install
cd $ROOT_DIR
KERNEL_VERSION=$(basename boot/vmlinu* | cut -d - -f 2-10)
tar -czvf $SCRIPT_DIR/kernel-${KERNEL_VERSION}.tar.gz .
